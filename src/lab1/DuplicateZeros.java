package lab1;
import java.util.Scanner;

public class DuplicateZeros {
    public static void main(String[] args) {
        // Scanner kb = new Scanner(System.in);
        int[] arr = { 1, 0, 2, 3, 0, 4, 5, 0 };

                for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                for (int j = arr.length - 1; j > i; j--) {
                    arr[j] = arr[j - 1];
                }

                arr[i + 1] = 0;
                i++;

            }

        }
        for (int i : arr) {
            System.out.print(i + " ");
        }

    }
}
