package lab1;
public class ArrayManipulation {
    public static void main(String[] args) {
        int[] numbers = { 5, 8, 3, 2, 7 };
        String[] names = { "Alice", "Bob", "Charlie", "David" };
        double[] values = new double[4];

        // Print the elements of the "numbers" array using a for loop.
        System.out.println("Elements of the numbers array:");
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        // Print the elements of the "names" array using a for-each loop.
        System.out.println("Elements of the names array:");
        for (String i : names){
            System.out.println(i);
        }
        // Initialize the elements of the "values" array with any four decimal values of
        // your choice.
        values[0] = 1.1;
        values[1] = 2.1;
        values[2] = 3.1;
        values[3] = 4.1;

        // Calculate and print the sum of all elements in the "numbers" array.
        int cal = 0;
        for (int i = 0; i < numbers.length; i++) {
            cal += numbers[i];

        }
        System.out.println("sum of all numbers = " + cal);

        // Find and print the maximum value in the "values" array.
        double max = 0.0;
        for (int i = 0; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            }

        }
        System.out.println("Maximum value = " + max);

        // Create a new string array named "reversedNames" with the same length as the
        // "names" array.
        System.out.println("the elements of the reversedNames array : ");
        String[] reversedNames = new String[names.length];
        for(int i=0;i<reversedNames.length;i++){
            reversedNames[i]=names[(names.length)-(i+1)];
            System.out.println(reversedNames[i]);
        }

    }
}


