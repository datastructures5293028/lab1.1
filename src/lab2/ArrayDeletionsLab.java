package lab2;

public class ArrayDeletionsLab {

    public static int[] deleteElementByIndex(int[] arr, int index) {
        if (index < 0 || index >= arr.length) {
            return arr;
        }

        int[] newArr = new int[arr.length - 1];
        int newIndex = 0;

        for (int i = 0; i < arr.length; i++) {
            if (i != index) {
                newArr[newIndex] = arr[i];
                newIndex++;
            }
        }

        return newArr;
    }
    public static int[] deleteElementByValue(int[] arr, int value) {
        int index = -1;
    
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                index = i;
                break;
            }
        }
    
        if (index == -1) {
            return arr;
        }
    
        int[] newArr = new int[arr.length - 1];
        int newIndex = 0;
    
        for (int i = 0; i < arr.length; i++) {
            if (i != index) {
                newArr[newIndex] = arr[i];
                newIndex++;
            }
        }
    
        return newArr;
    }
    
    

    public static void main(String[] args) {
        int[] arr = { 1, 2, 3, 4, 5 };
        int index = 2;
        int value = 4;
        // int[] newArr = new int[arr.length - 1];
        // int[] newArr1 = new int[newArr.length - 1];
        int[] updatedArr = deleteElementByIndex(arr, index);

        for (int i  : updatedArr) {
            System.out.print(i + " ");
        }
        System.out.println("");
        int[] updatedArr1 = deleteElementByValue(updatedArr, value);
        for (int i : updatedArr1 ){
            System.out.print(i+ " ");
        }

        
        

    }   

}

