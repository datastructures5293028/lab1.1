package lab3;

import java.util.Scanner;

public class sqrt {
    public static int sqrt(int x) {
        if (x == 0 || x == 1) {
            return x;
        }
                                                        // test case
        int left = 0, right = x;                        // left = 0 , right x = 4 
        while (left <= right) {                         // while (0<=4)
            int mid = left + (right - left) / 2;        // mid = 0 + (4-0)/2 --> mid = 2
            if (mid == x / mid) {                       // if mid == 4/2 --> 2
                return mid;                             //      return 2;
            } else if (mid < x / mid) {                 
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        
        return right;  // คืนค่าปัดลง
    }
    
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int x = kb.nextInt();
        System.out.println(sqrt(x));  
         
    }
}
